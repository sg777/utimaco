#!/bin/sh
derDirectory="der"
if [ ! -e $derDirectory ]
	then
	mkdir $derDirectory
fi

for i in *.pem
do
		var=$(echo $i | cut -d '.' -f 1)".der"
		var="./der/$var"
		openssl x509 -inform pem -outform der -in $i -out $var
done