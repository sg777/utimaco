#include<stdio.h>
#include<malloc.h>
#include<string.h>
#include<time.h>
#define SEQUENCE 0x30
#define INTEGER 0x02

struct TLV{
	unsigned char tag;
	unsigned char *lengthBytes;
	unsigned int length;
	unsigned char *value;
	unsigned int valueLength;
};
void encode(unsigned char *data,int inLength, unsigned char tagValue,struct TLV *tlv);
void display(struct TLV *tlv);
void tlvToStream(unsigned char *data, int length, struct TLV *tlv);

void version(struct TLV *tlv);
void serialNumber(struct TLV *tlv);
void signatureAlgorithm(struct TLV *tlv);
void issuer();
void issuerSubjectInfo(struct TLV *tlv,bool flag);
void validaity(struct TLV *tlv);
void subjectPublicKeyInfo(struct TLV *subjectPublicKeyInfo_tlv,unsigned char modulus[],int modulus_length,unsigned char exponent[],int exponent_length);
void extensions(struct TLV *extensions_tlv);
void tbsder(struct TLV *tlv);

void issuerSubjectInfoFromConfigForSS(struct TLV *tlv, bool flag,char *fileName);
void keyUsage(struct TLV *tlv);
void tlvToStream1(unsigned char **data, int *length, struct TLV *tlv);
void basicConstraints(struct TLV *tlv);