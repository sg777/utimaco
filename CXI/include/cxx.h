/**************************************************************************************************
 *
 * Filename           : cxx.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : CryptoServer Core Interface API Extension 
 *                      Beta / experimental functions 
 *                      Internal use only!!!
 *
 *************************************************************************************************/
#ifndef __CXX_H
#define __CXX_H

#include "cxi.h"

namespace cxi
{     
  class CXIAPI Cxx : public Cxi
  {
    public:            
      Key obj_create(
        int flags,
        PropertyList &keyTemplate
      );      
      
      ByteArray key_wrap(
        Key &key, 
        Key &wrapKey, 
        MechanismParameter &mechParam = NULL_REF(MechanismParameter), 
        ByteArray &iv = NULL_REF(ByteArray)
      );
  
      Key key_unwrap(
        int flags, 
        PropertyList &keyTemplate, 
        ByteArray &keyBlob, 
        Key &wrapKey, 
        MechanismParameter &mechParam = NULL_REF(MechanismParameter), 
        ByteArray &iv = NULL_REF(ByteArray)
      );              
  };
}


#endif
