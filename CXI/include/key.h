/**************************************************************************************************
 *
 * Filename           : key.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : CXI Key Object
 *
 *************************************************************************************************/
#ifndef __KEY_H
#define __KEY_H

#include "cxi.h"

namespace cxi
{  
  class CXIAPI Key : public ByteArray
  {    
    public:
      Key(void);
      Key(ByteArray b);
      Key(char *data, int len);
      virtual ~Key(void);
            
      /**
       * Key Types
       */
      enum types
      {
        TYPE_UNKNOWN = 0, //!< unknown key type
        TYPE_HANDLE,      //!< key handle: reference to internal key stored on CryptoServer
        TYPE_BLOB         //!< key blob: external key (encrypted with the CryptoServer's MBK)
      };
      
      int           getType();
      ByteArray     getUName();

      PropertyList  getProplist();
  };
}

#endif
