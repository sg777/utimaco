/**************************************************************************************************
 *
 * Filename           : mechparam.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Mechanism Parameter Constructor
 *
 *************************************************************************************************/
#ifndef __MECHPARAM_H
#define __MECHPARAM_H

#include "cxi.h"

namespace cxi
{    
  class CXIAPI MechanismParameter : public ByteArray
  {
    public:
      MechanismParameter(void);
      MechanismParameter(int mech);      
      MechanismParameter(int mech, int mgf_algo, char *label);      
      MechanismParameter(int mech, int mgf_algo, int salt_len);

      void set(int mech);      
      
      void setOAEP(int mech, int mgf_algo, char *label);      
      
      void setPSS(int mech, int mgf_algo, int salt_len); 
      
      void setECIES(int mech, int hash_algo, 
                    int crypt_algo, int crypt_mech, int crypt_len,
                    int mac_algo, int mac_mech, int mac_len,
                    char *p_secret1, int  l_secret1,
                    char *p_secret2, int  l_secret2);
                                              
      void setGCM(int mech, 
                  char *p_iv_init, int l_iv_init, 
                  char *p_ad, int l_ad,
                  int total_data_len, int total_ad_len);
      
      void setGMAC(int mech, 
                   char *p_iv_init, int l_iv_init,                    
                   int total_data_len);
      
      // operators
      void operator|=(const int mech);  
      
      virtual ~MechanismParameter(void);
  };
  
  // --------------------------------------------------------------------------------
  
  class CXIAPI MechParam
  { 
    public:
      int mech;
    
      MechParam(void);    
      MechParam(int mech); 
      MechanismParameter getEncoded(void); 
  };
  
  class CXIAPI MechParamOAEP : public MechParam
  {
    public:      
      int mgf_algo;
      std::string label;
      
      MechParamOAEP(void);
      MechParamOAEP(int mech, int mgf_algo, std::string &label);      
      MechanismParameter getEncoded(void);
  };
  
  class CXIAPI MechParamPSS : public MechParam
  {
    public:      
      int mgf_algo;
      int salt_len;
      
      MechParamPSS(void);
      MechParamPSS(int mech, int mgf_algo, int salt_len);      
      MechanismParameter getEncoded(void);
  };
  
  class CXIAPI MechParamECIES : public MechParam
  {
    public: 
      int hash_algo; 
      int crypt_algo; 
      int crypt_mech; 
      int crypt_len;
      int mac_algo; 
      int mac_mech; 
      int mac_len;
      ByteArray secret1;
      ByteArray secret2;
      
      MechParamECIES(void);
      MechParamECIES(int mech, int hash_algo, 
                     int crypt_algo, int crypt_mech, int crypt_len,
                     int mac_algo, int mac_mech, int mac_len,
                     ByteArray &secret1, ByteArray &secret2);
      MechanismParameter getEncoded(void);              
  };
  
  class CXIAPI MechParamGCM : public MechParam
  {
    public:
      ByteArray iv_init;
      ByteArray ad;
      int total_data_len;
      int total_ad_len;
      
      MechParamGCM(void);   
      MechParamGCM(int mech, ByteArray &iv_init, ByteArray &ad, int total_data_len, int total_ad_len);
      MechanismParameter getEncoded(void);
  };
  
  class CXIAPI MechParamGMAC : public MechParam
  {
    public:
      ByteArray iv_init;
      int total_data_len;
      
      MechParamGMAC(void);   
      MechParamGMAC(int mech, ByteArray &iv_init, int total_data_len);
      MechanismParameter getEncoded(void);
  };
}

#endif
