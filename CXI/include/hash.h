/**************************************************************************************************
 *
 * Filename           : hash.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Hash Creation
 *
 *************************************************************************************************/
#ifndef __HASH_H
#define __HASH_H

#include "cxi.h"

namespace cxi
{
  class CXIAPI Hash : public ByteArray
  {    
    private:
      int  hash_algo;
      void *hash_info;  

    public:
      Hash(void);
      Hash(int algo);
      virtual ~Hash(void);

      void init(int algo);
      void update(char *data, int len);
      void update(ByteArray &data);
      void final(void);      
  };
}

#endif
