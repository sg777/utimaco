/**************************************************************************************************
 *
 * Filename           : cxi.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : CryptoServer Core Interface API
 *
 *************************************************************************************************/
#ifndef __CXI_H
#define __CXI_H

#ifndef CXIAPI    
  #ifdef WIN32
    #ifdef CXIEXPORT
      #define CXIAPI __declspec(dllexport)    
    #else    
      #define CXIAPI __declspec(dllimport)
    #endif
  #else
    #if __GNUC__ >= 4
      #define CXIAPI __attribute__ ((visibility ("default")))
    #else
      #define CXIAPI 
    #endif
  #endif  
#endif

// type safe reference to non existing object
#define NULL_REF(cls) (*(cls*)0)

#include <vector>

#include "cxi_defs.h"
#include "exception.h"
#include "bytearray.h"
#include "item.h"
#include "keyblob.h"
#include "hash.h"
#include "mechparam.h"
#include "config.h"
#include "log.h"
#include "property.h"
#include "propertylist.h"
#include "key.h"
#include "keylist.h"
#include "keystore.h"
#include "util.h"

namespace cxi
{     
  class CXIAPI Cxi
  {          
    private:
      bool isCluster;              
      void init(void);      
    
    protected:
      void *p_sess_ctx;
      int  hCS;
      
      void open(char *device, int timeout = 60000, int ctimeout = 5000);
      void open(char **devices, int ndevs, int timeout = 60000, int ctimeout = 5000, int fallback_interval = 0);             
      
    public:
      /**       
       * \name Construction / Destruction
       */
      /*@{*/      
      Cxi(char *device, int timeout = 60000, int ctimeout = 5000);
      Cxi(char **devices, int ndevs, int timeout = 60000, int ctimeout = 5000, int fallback_interval = 0);
      Cxi(std::vector<std::string> devices, int timeout = 60000, int ctimeout = 5000, int fallback_interval = 0);      
      Cxi(Config &config);
      virtual ~Cxi(void);
      /*@}*/ 
      
      void set_msg_handler(void (*p_msg_func)(unsigned char *p_msg, int l_msg));
      void close(void);            
                        
      /** 
       * \brief Defines an additional error code/mask that causes the cluster to switch to another device instead of returning to the caller.
       *
       * The switch condition is fulfilled if:
       * \code
       * (error & mask) == code
       * \endcode
       * \see \ref Cxi::exec
       */
      typedef struct
      {
        unsigned int mask;  //!< error mask
        unsigned int code;  //!< error code
      }
      CLUSTER_ERROR;      
      
      /**    
       * \name Miscellaneous Functions
       */
      /*@{*/

      void exec(unsigned int fc, unsigned int sfc, 
                unsigned char *p_cmd, unsigned int l_cmd, 
                unsigned char **pp_answ, unsigned int *p_l_answ,
                CLUSTER_ERROR *errlist = NULL, unsigned int err_ct = 0);

      void free_answ(unsigned char *p_answ);
      
      static int get_version(void);
      int get_fw_version(void);      
      /*@}*/ 
      
      /**
       * \name Authentication
       */
      /*@{*/
      void  logon_sign(char *user, char *key, char *password = 0, bool keep_alive = false);
      void  logon_pass(char *user, char *password, bool keep_alive = false);      
      void  logoff(void);    
      int   get_auth_state(void);
      /*@}*/ 
                  
      /**
       * \name Key Management
       */
      /*@{*/      
      KeyList       key_list(PropertyList &keyTemplate = NULL_REF(PropertyList));      
      Key           key_generate(int flags, PropertyList &keyTemplate, int mech = -1);
      Key           key_create(int flags, PropertyList &keyTemplate, int mech = -1);
      Key           key_open(int flags, PropertyList &keyTemplate);
      Key           key_open(PropertyList &keyTemplate);
      void          key_delete(PropertyList &keyTemplate);
      void          key_delete(Key &key);      
      PropertyList  key_prop_get(Key &key, int properties[], int nprops);
      Key           key_prop_set(Key &key, PropertyList &propList);
      ByteArray     key_export(int type, Key &key, Key &exportKey, int mech = -1);
      Key           key_import(int flags, int type, PropertyList &keyTemplate, ByteArray &keyBlob, Key &importKey, int mech = -1);
      PropertyList  key_dsa_xgen(int psize, int qsize = 160, int mech = -1);
      Key           key_backup(Key &key);
      Key           key_restore(int flags, Key &key, PropertyList &keyTemplate = NULL_REF(PropertyList));
      /*@}*/

      /**
       * \name Cryptography
       */
      /*@{*/
      ByteArray crypt (int flags, Key &key, MechanismParameter &mechParam, ByteArray &data, ByteArray &iv = NULL_REF(ByteArray), ByteArray &tag = NULL_REF(ByteArray));      
      ByteArray crypt (Key &key, MechanismParameter &mechParam, ByteArray &data, ByteArray &iv = NULL_REF(ByteArray));
      std::vector<ByteArray> bulk_crypt(Key &key, MechanismParameter &mechParam, std::vector<ByteArray> &data);

      ByteArray sign(int flags, Key &key, MechanismParameter &mechParam, ByteArray &data, ByteArray &iv = NULL_REF(ByteArray));
      ByteArray sign(Key &key, MechanismParameter &mechParam, ByteArray &data, ByteArray &iv = NULL_REF(ByteArray));
      std::vector<ByteArray> bulk_sign(Key &key, MechanismParameter &mechParam, std::vector<ByteArray> &data);

      bool verify(int flags, Key &key, MechanismParameter &mechParam, ByteArray &data, ByteArray &signature, ByteArray &iv = NULL_REF(ByteArray));
      void verify(Key &key, MechanismParameter &mechParam, ByteArray &data, ByteArray &signature, ByteArray &iv = NULL_REF(ByteArray));
      
      ByteArray rnd_gen(int len, int mech = -1);
      
      ByteArray hash_compute(int flags, MechanismParameter &mechParam, ByteArray &data, ByteArray &info = NULL_REF(ByteArray), Key &key = NULL_REF(Key));
      
      ByteArray secret_agree(int flags, Key &privateKey, Key &publicKey, MechanismParameter &mechParam = NULL_REF(MechanismParameter));
      /*@}*/

  };  
}


// --- BEGIN ERROR CODES ---

#define E_CXI_API                       0xB920      // CryptoServer Core API Cxi

#define E_CXI_API_ALLOC                 0xB9200001  // memory allocation failed
#define E_CXI_API_PARAM                 0xB9200002  // invalid parameter
#define E_CXI_API_PARAM_LEN             0xB9200003  // invalid parameter length
#define E_CXI_API_PARAM_RANGE           0xB9200004  // parameter out of range
#define E_CXI_API_BUF_SIZE              0xB9200005  // buffer size too small
#define E_CXI_API_ANSW_LEN              0xB9200006  // invalid answer length
#define E_CXI_API_ANSW_DATA             0xB9200007  // invalid format of answer data
#define E_CXI_API_STRING_TERM           0xB9200008  // unterminated string
#define E_CXI_API_STRING_CONV           0xB9200009  // string conversion failed
#define E_CXI_API_NOT_FOUND             0xB920000A  // object/item not found
#define E_CXI_API_COMPARE               0xB920000B  // compare failed
#define E_CXI_API_ALGO                  0xB920000C  // invalid algorithm
#define E_CXI_API_STATE                 0xB920000D  // invalid state
#define E_CXI_API_FILE                  0xB920000E  // file error
#define E_CXI_API_USER_NOT_FOUND        0xB920000F  // user does not exist
#define E_CXI_API_NOT_SUPPORTED         0xB9200010  // operation not supported
#define E_CXI_API_INVALID_KEY           0xB9200011  // invalid key
#define E_CXI_API_IO                    0xB9200012  // I/O error
#define E_CXI_API_LOG                   0xB9200013  // log access error
#define E_CXI_API_DB                    0xB9200014  // database access error
#define E_CXI_API_ASN1_FORMAT           0xB9200015  // invalid ASN.1 format
#define E_CXI_API_MEM_CORR              0xB9200016  // memory corruption

#define E_CXI_API_SYSTEM                0xB9201     // system error

// --- END ERROR CODES ---

#define CXI_SYS_ERR(errno)              ((E_CXI_API_SYSTEM << 12) | ((errno) & 0xFFF))

#endif
