/**************************************************************************************************
 *
 * Filename           : config.h
 *
 * Author             : Dipl. Ing. Sven Kaltschmidt
 *                      Utimaco Safeware AG
 *
 * Description        : Configuration Object / Parser
 *
 *************************************************************************************************/
#ifndef __CONFIG_H
#define __CONFIG_H

#ifdef WIN32
  #pragma warning (disable: 4786)
  #pragma warning (disable: 4251)
#endif

#include <vector>
#include <string>
#include <map>

#include "cxi.h"

namespace cxi
{    
  class CXIAPI Config
  {    
    private:
      std::map<std::string,ByteArray> items;

      void parse(std::string filename);      

    public:      
      Config(void);
      Config(std::string filename);
      virtual ~Config(void);
            
      void dump(void);      
      bool exist(std::string key);
      
      void add(std::string key, ByteArray &value);
      void addString(std::string key, std::string value);
      void addInt(std::string key, int value);
      
      ByteArray   get(std::string key, ByteArray def);
      std::string getString(std::string key, std::string def);
      int         getInt(std::string key, int def);
      
      std::vector<std::string> getStringValues(std::string key);
  };
}

#endif
